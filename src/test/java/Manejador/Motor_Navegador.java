package Manejador;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Motor_Navegador {
	
	public String baseURL = "https://www.google.com/";
	public WebDriver driver;


	@AfterClass
	  public void Salir() throws InterruptedException {
		driver.close();
	    driver.quit();
	  }	
	
	
	@BeforeClass
	public void Abrir() {
		System.setProperty("webdriver.chrome.driver", "..\\Search\\library\\chromedriver.exe");
		driver= new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

}
