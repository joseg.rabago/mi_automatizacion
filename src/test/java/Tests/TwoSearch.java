package Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import Manejador.Motor_Navegador;
import Utilerias.SeleniumComunes;

public class TwoSearch  extends Motor_Navegador {
	 	

			  @Test(description = "primera busqueda", priority = 1,  enabled = true)
				  public void BusquedaPrimera() throws InterruptedException  {
					driver.get(baseURL);
					driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).click(); 
				    driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).sendKeys("choix existe memes");
				    Thread.sleep(1000);
				    driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).sendKeys(Keys.ENTER);
				    Thread.sleep(1000);
				    driver.findElement(By.xpath("(//a[contains(.,'Im�genes')])[1]")).click();
				    Thread.sleep(3000);
				    SeleniumComunes.capturarPantalla(driver,"C:\\Users\\jose.rabago\\Desktop\\MyAcademy\\DosBusquedas\\Capturas\\primera busqueda.jpg");


				  	}  
			
			  @Test(description = "segunda busqueda", priority = 2,  enabled = true)
				  public void BusquedaSegunda() throws InterruptedException  {
					driver.get(baseURL);
					driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).click(); 
				    driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).sendKeys("choix no existe memes");
				    Thread.sleep(1000);
				    driver.findElement(By.xpath("(//input[contains(@aria-label,'Buscar')])[1]")).sendKeys(Keys.ENTER);
				    Thread.sleep(1000);
				    driver.findElement(By.xpath("(//a[contains(.,'Im�genes')])[1]")).click();
				    Thread.sleep(3000);
				    SeleniumComunes.capturarPantalla(driver,"C:\\Users\\jose.rabago\\Desktop\\MyAcademy\\DosBusquedas\\Capturas\\segunda busqueda.jpg");

				    }  

}


