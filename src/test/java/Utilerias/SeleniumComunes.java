package Utilerias;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class SeleniumComunes {
	static WebDriver driver;

	public static void capturarPantalla ( WebDriver driver, String nombreArchivo) {
		try {
		TakesScreenshot pantallazo =((TakesScreenshot) driver);
		File SrcFile = pantallazo.getScreenshotAs(OutputType.FILE);
		File DestFile = new File(nombreArchivo);
		try {
			FileUtils.copyFile(SrcFile,DestFile);
			} catch (IOException e) {
			e.printStackTrace();
			}
			} catch (WebDriverException e) {

			e.printStackTrace();
			}

			}
}
